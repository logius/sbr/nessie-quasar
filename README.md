# Nessie

_a XBRL-Taxonomy viewer (and more)_

This frontend is written with the aid of Quasar, which is build around Vue.js. It's a tool to provide views on a XBRL Taxonomy. If there are tables defined inside the entrypoint we render this table in full.
You can run this frontend on your self hosted backend or you can choose to use our public API, currently hosted at: https://kc-xbrl.cooljapan.nl/api/docs

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://v2.quasar.dev/quasar-cli/quasar-conf-js).

Running Nessie
To test your build locally, you can run a local 'production'-server from the dist/spa directory.
```bash
quasar serve --proxy prod_proxy.js dist/spa/
```
The contents of prod_proxy.js should point to a responsive api-server (whether localhost or kc-xbrl.cooljapan.nl)
```javascript
module.exports = [
      {
        path: '/api',
        rule: { target: 'http://localhost:8000', pathRewrite: {'api': ''} }
      }
    ]
```

