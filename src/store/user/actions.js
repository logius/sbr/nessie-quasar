/*
export function someAction (context) {
}
*/

import {api} from "boot/axios";
import { Cookies } from 'quasar'

export const DoLogin = (state, creds) => {
  api
    .post('/admin/login/token', creds)
    .then(response => {
      state.commit('LOGIN_OK', response.data.access_token)
      GetUser(state, creds.get('username'))
      Cookies.set('kc_token', response.data.access_token, {sameSite: 'Strict'})
    })
    .catch(function (error) {
      if (error.response && error.response.data && error.response.data.detail) {
        state.commit('LOGIN_ERROR', error.response.data.detail)
      } else {
        state.commit('LOGIN_ERROR', "Login failed (generic)")
      }
    })
}

export const set_token = (state, token) => {
  this.state.commit('LOGIN_OK', token)
}

export const GetUser = (state, username) => {
  if (username) {
    api
      .get('/admin/current_user', {params: {username: username}})
      .then(response => {
        if (response.data) {
          state.commit("STORE_USER", response.data)
        }
      })
  }
}
