/*
export function someMutation (state) {
}
*/
export const LOGIN_OK = (state, data) => {
  state.token = data
  state.error = false
}
export const LOGIN_ERROR = (state, data) => {
  state.error = true
  state.token = ""
  state.detail = data
}
export const LOGOUT = (state, data) => {
  state.error = false
  state.token = ""
  state.detail = ""
  state.username = ""
}
export const STORE_USER = (state, data) => {
  state.User = data
}
