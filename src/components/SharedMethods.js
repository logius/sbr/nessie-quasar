let global_lang = "nl"
let global_nt_version = "nt16"

export default {
  label_nice_role: function (cell) {
    /* accepts a role_uri and returns the last part of the path */
    if (cell.preferred_label) {
      let parts = cell.preferred_label.split('/')
      return parts[parts.length - 1]
    } else if (cell.link_role) {
      let parts = cell.link_role.split('/')
      return parts[parts.length - 1]
    } else {
      return 'label'
    }
  },
  get_label: function (node) {
    let role = this.label_nice_role(node) // only presentation_nodes and members knows 'preferred_label'
    let labels = undefined
    if (node.definition && !node.labels) {
      return node.definition
    }
    // now where are the labels
    if (node.labels) {
      labels = node.labels
    } else if (node.concept) {
      labels = node.concept.labels
    } else if (node.rulenode) {
      labels = node.rulenode.labels
    } else if (node.aspect) {
      labels = node.aspect.labels // auw
    } else {
      return ""
    }
    let label = labels.find(x => (x.lang === global_lang && x.link_role.split('/').slice(-1)[0] === role))
    if (label) {
      return label.label_text
    } else {
      return ""
    }
  },
  show_label: function (node) {
    let label = this.get_label(node)
    if (label > '') {
      return label
    }
    if (node.definition) {
      return '<div class="definition">' + node.definition + " <abbr title='Geen label gevonden, dit is de definitie van de linkrole'> * </abbr></div>"
    }
    // still here? thenwehave no label
    let id = '_merde_'
    if (node.concept) {
      id = node.concept.id
    } else if (node.id) {
      id = node.id
    } else {
      id = node.hc_role_uri
    }
    return '<div class="no_label">' + id + "</div>"
  },
  set_lang: function (newlang) {
    if (newlang !== global_lang) {
      global_lang = newlang
    }
  },
  get_lang: function () {
    return global_lang
  },
  set_nt_version: function (new_version) {
    if (new_version !== global_nt_version) {
      global_nt_version = new_version
    }
  },
  get_nt_version: function () {
    return global_nt_version
  },
  nt_version_for_url: function (nt_version) {
    if (nt_version.includes("_")) {
      let parts = nt_version.split("_")
      return parts[0] + "." + parts[1][0]
    }
    return nt_version
  }
}
