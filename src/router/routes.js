const routes = [
  {
    path: '/',
    component: () => import('layouts/LandingPage.vue'),
    children: [
      {path: '', component: () => import('pages/Welcome.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name',
    component: () => import('layouts/DtsViewer.vue'),
    props: true,
    children: [
      {path: "", component: () => import('components/TocDetail')}
    ]
  },
  {
    path: '/concepts',
    component: () => import('layouts/ConceptViewer'),
    children: [
      {path: "concept/:concept_id", name: "concept_detail", component: () => import('components/ConceptDetail.vue')},
      {path: "", component: () => import('components/ConceptDetail.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/concepts',
    component: () => import('layouts/ConceptViewer'),
    props: true,
    children: [
      {
        path: "concept/:concept_id",
        name: "concept_detail",
        component: () => import('components/ConceptDetail.vue'),
        props: true
      },
      {path: "", component: () => import('components/ConceptDetail.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/tables',
    component: () => import('layouts/TableViewer'),
    props: true,
    children: [
      {
        path: "table/:table_id",
        name: "table_detail",
        component: () => import('components/TableDetail.vue'),
        props: true,
      },
      {path: "", component: () => import('components/TableDetail.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/definition',
    component: () => import('layouts/HypercubeViewer'),
    props: true,
    children: [
      {
        path: ":hc_role_uri",
        name: "hypercube_detail",
        props: true,
        component: () => import('components/DefinitionLinkDetail')
      },
      {path: "", component: () => import('components/DefinitionLinkDetail')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/linkroles',
    component: () => import('layouts/LinkroleViewer'),
    props: true,
    children: [
      {
        path: "linkroles/:linkrole_id/:linkrole_role_uri",
        name: "linkrole_detail",
        component: () => import('components/LinkroleDetail.vue'),
        props: true
      },
      {path: "", component: () => import('components/LinkroleDetail.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/itemtypes',
    component: () => import('layouts/ItemtypeViewer.vue'),
    props: true,
    children: [
      {
        path: ':itemtype_name',
        name: 'itemtype_detail',
        component: () => import('components/ItemtypeDetail.vue'),
        props: true
      },
      {path: "", component: () => import('components/ItemtypeDetail.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/labels',
    component: () => import('layouts/LabelViewer.vue'),
    props: true,
    children: [
      {
        path: ':label_id/:label_text/:link_role/:label_lang',
        name: 'label_detail',
        component: () => import('components/LabelDetail.vue'),
        props: true
      },
      {path: "", component: () => import('components/LabelDetail.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/references',
    component: () => import('layouts/ReferenceViewer.vue'),
    props: true,
    children: [
      {
        path: ':reference_id',
        name: 'reference_detail',
        component: () => import('components/ReferenceDetail.vue'),
        props: true
      },
      {path: "", component: () => import('components/ReferenceDetail.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/assertions',
    component: () => import('layouts/AssertionViewer.vue'),
    props: true,
    children: [
      {
        path: ':assertion_id',
        name: 'assertion_detail',
        component: () => import('components/AssertionDetail.vue'),
        props: true
      },
      {path: "", component: () => import('components/AssertionDetail.vue')}
    ]
  },

  {
    path: '/dts/:nt_version/:dts_name/dimensions',
    component: () => import('layouts/DimensionViewer.vue'),
    props: true,
    children: [
      {
        path: ':rowid',
        name: 'dimension_detail',
        component: () => import('components/DimensionDetail.vue'),
        props: true
      },
      {path: "", component: () => import('components/DimensionDetail.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/arcs',
    component: () => import('layouts/ArcViewer.vue'),
    props: true,
    children: [
      {
        path: ':arcrole/:object_id',
        name: 'arc_detail',
        component: () => import('components/ArcDetail.vue'),
        props: true
      },
      {
        path: ':arcrole',
        name: 'arc_detail_incomplete',
        component: () => import('components/ArcDetail.vue'),
        props: true
      },
      {path: "", component: () => import('components/ArcDetail.vue')}
    ]
  },
  {
    path: '/dts/:nt_version/:dts_name/presentations',
    component: () => import('layouts/PresntationHierarchyViewer.vue'),
    props: true,
    children: [
      {
        path: ':rowid',
        name: 'presentation_detail',
        component: () => import('components/PresentationHierarchyDetail.vue'),
        props: true
      },
      {path: "", component: () => import('components/PresentationHierarchyDetail.vue')}
    ]
  },

  {
    path: '/search/:query',
    name: 'search',
    component: () => import('layouts/SearchViewer'),
    children: [
      {path: "", name: "search_results", component: () => import('components/SearchResults')}
    ]
  },
  {
    path: '/admin',
    name: 'dashboard',
    component: () => import('layouts/Dashboard'),
    children: [
      {path: 'compare',
       name: 'admin_compare',
       component: () => import('components/CompareTax.vue')},
      {path: 'parse_url',
       name: 'admin_parse_url',
      component: () => import('components/ParseByUrl')},
      {path: 'taxonomy_package',
      name: 'admin_taxonomy_package',
      component: () => import('components/TaxonomyPackage')},
      {path: "", name: "dasboard_detail_default", component: () => import('components/DashboardDetail')}
    ]
  },
  // {
  //   path: '/test',
  //   component: () => import('layouts/test')
  // },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
