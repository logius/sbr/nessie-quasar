# Steps of deployment
# 1 inside boot/axios.js change the api-url from localhost (development to the desired api-server

# 2 Build the project
quasar build

# 3 goto the deployment repository, which distributes the 'production' build
# for met this is hosted on gitlab https://gitlab.com/xiffy/nessie-demo/

cd ../../../deploy/nessie-demo/

# $ empty the 'public' folder, the files are generated with different names (which prevents all kind of caching mysteries)
# the consequence of this decision is that the list of files just grows if you don't clean up

rm -rf public/*

# 6 copy the build inside of public
cp -rup  ~/develop/quasar/nessie-design/dist/spa/* public/

# Good,
# 7  inform git we changed some things
git add *

# git status
# 8 commit with some message
git commit -m "Deploy <some information>"

# 9 bring it to gitlab
git push
# 10 wait a couple of seconds
sleep 30
# 11 it's published at: https://xiffy.gitlab.io/nessie-demo/
