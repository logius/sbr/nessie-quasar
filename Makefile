all: nessie

nessie:
	@echo "Builds and copies the single page application SPA to the webroot"

	git checkout master
	git pull
	npm install
	quasar build
	@echo "Cleaning webdir js/css"
	@rm /var/www/html/js/*js
	@rm /var/www/html/css/*css
	@echo "Publishing"
	cp -rup dist/spa/* /var/www/html

.PHONY: list
list:
	@LC_ALL=C $(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

