FROM nginxinc/nginx-unprivileged
COPY dist/spa /usr/share/nginx/html
COPY container.conf/default.conf /etc/nginx/conf.d/default.conf
COPY container.conf/nginx.conf /etc/nginx/nginx.conf

